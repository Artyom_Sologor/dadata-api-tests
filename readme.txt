Описание реализации тестового решения автоматизации API dadata.ru.

Тестовое решение автоматизации API dadata.ru  включает в себя покрытие тестами следующих методов:
1. Метод геокодирования (координаты по адресу)  - TestsCleanerAddress  (https://dadata.ru/api/geocode/)
2. Метод стандартизации составных записей - TestsCleanerRecord (https://dadata.ru/api/clean/record/)
3. Метод поиска почтового отделения по его индексу - TestsPostalUnitFindById (https://dadata.ru/api/suggest/postal_unit/)
4. Метод поиска ближайшего почтового отделения по координатам - TestsPostalUnitGeolocate (https://dadata.ru/api/suggest/postal_unit/)
5. Метод полнотекстового поиска отделения по индексу или адресу  - TestsPostalUnitSuggest (https://dadata.ru/api/suggest/postal_unit/)

!!!ДЛЯ ЗАПУСКА ТЕСТИРОВАНИЯ ТРЕБУЕТСЯ ТОЛЬКО ЗАМЕНИТЬ КОНСТАНТЫ API_KEY И X_SECRET В КЛАССЕ AuthConstants НА ВАШИ ТОКЕНЫ, ПОЛУЧЕННЫЕ С DADATA.RU!!!

В классе config/Сonfig хранятся:
1. Базовые спецификации для всех тестов
2. Спецификации для тестов с различным содержимым заголовков
Также класс config/Сonfig выполняет функцию сборщика воедино URL запроса из констант constants/AuthConstans и constants/Constants.

В классе constants/AuthConstans хранятся API ключи для доступа к dadata.ru

В классе constants/Constants хранятся:
1. Константы server
2. Константы path
3. Контанты action

В классе pojos/AddressGeoPojo хранится модель класса для использования в запросах при необходимости

В классе pojos/SuggestionsPostUnitPojo хранится модель класса для использования в запросах при необходимости

В классе test_data/RequestJsons хранятся подготовленные для тел запросов json переменные

В папке recources хрянятся файлы JSON  для валидации в соответствующих тестах

