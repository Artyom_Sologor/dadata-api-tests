import config.Config;
import org.testng.annotations.Test;
import pojos.AddressGeoPojo;

import java.util.List;
import static constants.Constants.ActionsCleaner.DADATA_POST_ADDRESS;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static test_data.RequestsJsons.*;

// https://cleaner.dadata.ru/api/v1/clean/address
public class TestsCleanerAddress extends Config {
// Positive tests
    @Test
    public void validRequest() {
        given()
                .spec(baseSpecCleaner).body(addressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(200).log().body();
    }

    @Test
    public void validJsonInResponse() {
        given()
                .spec(baseSpecCleaner).body(addressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().body(matchesJsonSchemaInClasspath("cleanerAddressJsonSchema.json")).log().body();
    }

    @Test
    public void validResultFieldInResponse() {
        given()
                .spec(baseSpecCleaner).body(addressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().body("[0].result", equalTo("г Москва, ул Сухонская, д 11")).log().body();
    }

    @Test
    public void validValueGeoFieldsValuesInResponse() {
        double geoLat = 0;
        double geoLon = 0;

        List<AddressGeoPojo> geoFields;

        try {
             geoFields = given()
                    .spec(baseSpecCleaner).body(addressJson).log().uri()
                    .when().post(DADATA_POST_ADDRESS)
                    .then()
                    .extract().jsonPath().getList("", AddressGeoPojo.class);
            geoLat = geoFields.get(0).getGeo_lat();
            geoLon = geoFields.get(0).getGeo_lon();
        }
        catch(Exception e) {
            System.out.println("Deserialization error " + e);
        }
//        "geo_lat": "55.8782557", "geo_lon": "37.65372",
        assertThat(geoLat, is(closeTo(55.878448474228904, 0.005)));
        assertThat(geoLon, is(closeTo(37.65366829858731, 0.005)));
    }

    // Negative tests
    @Test
    public void noBodyInRequest() {
        given()
                .spec(baseSpecCleaner).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(400).log().body();
    }

    @Test
    public void notPostMethodInRequest() {
        given()
                .spec(baseSpecCleaner).body(addressJson).log().uri()
                .when().get(DADATA_POST_ADDRESS)
                .then().statusCode(405).log().body();
    }


    @Test
    public void fieldValue300CharsInRequest() {
        given()
                .spec(baseSpecCleaner).body(_300CharsAddressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(400).log().body();
    }

    @Test
    public void invalidJsonInRequest() {
        given()
                .spec(baseSpecCleaner).body(addressInvalidJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(400).log().body();
    }

    @Test
    public void noApiKeyInRequest() {
        given()
                .spec(noApiKeyHeaderCleanerSpec).body(addressJson)
                .log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(401).log().body();
    }

    @Test
    public void noSecretKeyInRequest() {
        given()
                .spec(noSecretKeyHeaderCleanerSpec).body(addressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(401).log().body();
    }

    @Test
    public void headerCaseInsensitivityInRequest() {
        given()
                .spec(caseInsensitivityCleanerSpec).body(addressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noHeaderContentTypeInRequest() {
        given()
                .spec(noContentTypeHeaderCleanerSpec).body(addressJson).log().uri()
                .when().post(DADATA_POST_ADDRESS)
                .then().statusCode(415).log().body();
    }

}
