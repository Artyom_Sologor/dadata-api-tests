import config.Config;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import pojos.AddressGeoPojo;
import pojos.SuggestionsPostUnitPojo;

import java.util.List;

import static constants.Constants.ActionsSuggestions.DADATA_POST_SUGGEST;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static test_data.RequestsJsons.*;

// https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/postal_unit
public class TestsPostalUnitSuggest extends Config {
    //    Positive tests
    @Test
    public void validRequestAddress() {
        given()
                .spec(baseSpecSuggestions).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().status();
    }

    @Test
    public void validRequestPostalCode() {
        given()
                .spec(baseSpecSuggestions).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void validJsonInResponse() {
        given()
                .spec(baseSpecSuggestions).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().body(matchesJsonSchemaInClasspath("postalUnitJsonSchema.json")).log().body();
    }

    @Test
    public void validGeoFieldsValues() {
        double geoLat = 0;
        double geoLon = 0;

        List<AddressGeoPojo> geoFields;
        JSONObject queryJson = new JSONObject();
        queryJson.put("query", "дежнева 2а");

        try {
            geoFields = given()
                    .spec(baseSpecSuggestions).body(queryJson).log().uri()
                    .when().post(DADATA_POST_SUGGEST)
                    .then()
                    .extract().jsonPath().getList("suggestions.data", AddressGeoPojo.class);
            geoLat = geoFields.get(0).getGeo_lat();
            geoLon = geoFields.get(0).getGeo_lon();
        }
        catch(Exception e) {
            System.out.println("Deserialization error " + e);
        }
//        "geo_lat": 55.872127, "geo_lon": 37.65122
        assertThat(geoLat, is(closeTo(55.87202370631399, 0.005)));
        assertThat(geoLon, is(closeTo(37.65191582742296, 0.005)));
    }

    @Test
    public void notNullFieldsValues() {
        String postalCode = null;
        Double geoLat = null;
        Double geoLon = null;

        List<SuggestionsPostUnitPojo> data = null;
        JSONObject queryJson = new JSONObject();
        queryJson.put("query", "дежнева 2а");
        try {
            data = given()
                    .spec(baseSpecSuggestions).body(queryJson).log().uri()
                    .when().post(DADATA_POST_SUGGEST)
                    .then()
                    .extract().jsonPath().getList("suggestions.data", SuggestionsPostUnitPojo.class);
            postalCode = data.get(0).getPostalCode();
            geoLat = data.get(0).getGeoLat();
            geoLon = data.get(0).getGeoLon();
        } catch (Exception e){
            System.out.println("Deserialization error " + e);
        }
        System.out.println(data.get(0));
        assertThat(postalCode, is(notNullValue()));
        assertThat(geoLat, is(notNullValue()));
        assertThat(geoLon, is(notNullValue()));
    }


        // Negative tests
    @Test
    public void noBodyInRequest() {
        given()
                .spec(baseSpecSuggestions).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void wrongFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("queryy", "127642");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void upperCaseFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("QUERY", "127642");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void emptyValueFieldInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("query", "");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void specSymbolsQueryFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("query!@#$%^&*()", "127642");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void specSymbolsQueryFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("query", "127642!@#$%^&*()");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }
    @Test
    public void charsQueryFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("query", "abcdedg");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void tooLongPostalCodeInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("query", "1234567890");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void fieldValue299CharsInRequest() {
        given()
                .spec(baseSpecSuggestions).body(_299CharsPostalUnitJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }


    @Test
    public void fieldValue300CharsInRequest() {
        given()
                .spec(baseSpecSuggestions).body(_300CharsPostalUnitJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void fieldValue301CharsInRequest() {
        given()
                .spec(baseSpecSuggestions).body(_301CharsPostalUnitJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(413).log().body();
    }

    @Test
    public void noApiKeyInRequest() {
        given()
                .spec(noApiKeyHeaderSuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(401).log().status();
    }

    @Test
    public void noSecretKeyInRequest() {
        given()
                .spec(noSecretKeyHeaderSuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void headerCaseInsensitivityInRequest() {
        given()
                .spec(caseInsensitivitySuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noContentTypeInRequest() {
        given()
                .spec(noContentTypeHeaderSuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_SUGGEST)
                .then().statusCode(415).log().body();
    }

}
