import config.Config;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static constants.Constants.ActionsSuggestions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.*;
import static test_data.RequestsJsons.geolocateJson;
import static test_data.RequestsJsons.suggestJson;

public class TestsPostalUnitGeolocate extends Config {
    // Positive tests
    @Test
    public void validRequest() {
        given()
                .spec(baseSpecSuggestions).body(geolocateJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().status();
    }

    @Test
    public void validPostalCodeValueInResponse() {
        given()
                .spec(baseSpecSuggestions).body(geolocateJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().body("suggestions[0].data.postal_code", equalTo("127642")).log().body();
    }

    @Test
    public void validJsonInResponse() {
        given()
                .spec(baseSpecSuggestions).body(geolocateJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().body(matchesJsonSchemaInClasspath("postalUnitJsonSchema.json")).log().body();
    }

    @Test
    public void lowRadiusFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "600");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().body("suggestions[0].data.postal_code", equalTo(null)).statusCode(200).log().body();
    }

    // Negative tests
    @Test
    public void noBodyInRequest() {
        given()
                .spec(baseSpecSuggestions).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void notPostMethodInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().get(DADATA_POST_GEOLOCATE)
                .then().body("suggestions[0].data", equalTo(null)).log().body();
    }

    @Test
    public void noRequiredLatFieldInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noRequiredLonFieldInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noRequiredRadiusFieldInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void wrongLatFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat1", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void wrongLonFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon1", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void wrongRadiusFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters1", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void upperCaseLatFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("LAT", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().body("suggestions[0].data.postal_code", equalTo(null)).statusCode(200).log().body();
    }

    @Test
    public void upperCaseLonFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("LON", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().body("suggestions[0].data.postal_code", equalTo(null)).statusCode(200).log().body();
    }

    @Test
    public void upperCaseRadiusFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("RADIUS_METERS", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().body("suggestions[0].data.postal_code", equalTo(null)).statusCode(200).log().body();
    }

    @Test
    public void specSymbolsLatFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat!@#$%^&*()", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void specSymbolsLonFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon!@#$%^&*()", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void specSymbolsRadiusFieldNameInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters!@#$%^&*()", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void charsLatFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "abcdefg");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(400).log().body();
    }

    @Test
    public void charsLonFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "abcdefg");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(400).log().body();
    }

    @Test
    public void charsRadiusFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "abcdefg");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(400).log().body();
    }

    @Test
    public void specSymbolsLatFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878!@#$%^&*()");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(400).log().body();
    }

    @Test
    public void specSymbolsLonFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653!@#$%^&*()");
        queryJson.put("radius_meters", "1000");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(400).log().body();
    }

    @Test
    public void specSymbolsRadiusFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000!@#$%^&*()");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(400).log().body();
    }

    @Test
    public void doubleRadiusFieldValueInRequest() {
        JSONObject queryJson = new JSONObject();
        queryJson.put("lat", "55.878");
        queryJson.put("lon", "37.653");
        queryJson.put("radius_meters", "1000.25");

        given()
                .spec(baseSpecSuggestions).body(queryJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noApiKeyInRequest() {
        given()
                .spec(noApiKeyHeaderSuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(401).log().status();
    }

    @Test
    public void noSecretKeyInRequest() {
        given()
                .spec(noSecretKeyHeaderSuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void headerCaseInsensitivityInRequest() {
        given()
                .spec(caseInsensitivitySuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noContentTypeInRequest() {
        given()
                .spec(noContentTypeHeaderSuggestionsSpec).body(suggestJson).log().uri()
                .when().post(DADATA_POST_GEOLOCATE)
                .then().statusCode(415).log().body();
    }
}
