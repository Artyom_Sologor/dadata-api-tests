import config.Config;
import org.testng.annotations.Test;

import static constants.Constants.ActionsCleaner.DADATA_POST_RECORD;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static test_data.RequestsJsons.*;

// https://cleaner.dadata.ru/api/v1/clean/
public class TestsCleanerRecord extends Config {
    //    Positive tests
    @Test
    public void validRequest() {
        given()
                .spec(baseSpecCleaner).body(recordJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(200).log().status();
    }

    @Test
    public void fiveFeildsInRequest() {
        given()
                .spec(baseSpecCleaner).body(recordFiveFieldsJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(200).log().body();
    }

    @Test
    public void validJsonInResponse() {
        given()
                .spec(baseSpecCleaner).body(recordJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().body(matchesJsonSchemaInClasspath("cleanerRecordJsonSchema.json")).log().body();
    }

    @Test
    public void validJsonInResponse1() {
        given()
                .spec(baseSpecCleaner).body(recordWithAsIsFieldJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(200).log().body();
    }
    //    Negative tests

    @Test
    public void recordWithUndocumentFieldInRequest() {
        given()
                .spec(baseSpecCleaner).body(recordWithUndocomentFieldJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(400).log().body();
    }
    @Test
    public void fieldValue300CharsInRequest() {
        given()
                .spec(baseSpecCleaner).body(_300CharsRecordJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(500).log().body();
    }

    @Test
    public void invalidJsonInRequest() {
        given()
                .spec(baseSpecCleaner).body(recordInvalidJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(400).log().body();
    }

    @Test
    public void noApiKeyInRequest() {
        given()
                .spec(noApiKeyHeaderCleanerSpec).body(recordJson)
                .log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(401).log().body();
    }

    @Test
    public void noSecretKeyInRequest() {
        given()
                .spec(noSecretKeyHeaderCleanerSpec).body(recordJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(401).log().body();
    }

    @Test
    public void headerCaseInsensitivityInRequest() {
        given()
                .spec(caseInsensitivityCleanerSpec).body(recordJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(200).log().body();
    }

    @Test
    public void noContentTypeInRequest() {
        given()
                .spec(noContentTypeHeaderCleanerSpec).body(recordJson).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(415).log().body();
    }

    @Test
    public void notPostMethodInRequest() {
        given()
                .spec(baseSpecCleaner).body(recordJson).log().uri()
                .when().get(DADATA_POST_RECORD)
                .then().statusCode(405).log().body();
    }
    @Test
    public void jsonWithErrorInRequest() {
        given()
                .spec(baseSpecCleaner).body(recordJsonWithError).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().statusCode(500).log().body();
    }

    @Test
    public void noBodyInRequest() {
        given()
                .spec(baseSpecCleaner).log().uri()
                .when().post(DADATA_POST_RECORD)
                .then().body("error", equalTo("Bad Request")).statusCode(400).log().body();
    }
}
