package constants;

public class Constants {
    public static class RunVariable {
        public static String serverCleaner = Servers.DADATA_URL_CLEANER;
        public static String pathCleaner = Path.DADATA_PATH_CLEANER;
        public static String serverSuggestions = Servers.DADATA_URL_SUGGESTIONS;
        public static String pathSuggestions = Path.DADATA_PATH_SUGGESTIONS;

    }
    public static class Servers {
        public static String DADATA_URL_CLEANER = "https://cleaner.dadata.ru/";
        public static String DADATA_URL_SUGGESTIONS = "https://suggestions.dadata.ru/";

    }
    public static class Path {
        public static String DADATA_PATH_CLEANER = "api/v1/";
        public static String DADATA_PATH_SUGGESTIONS = "suggestions/api/4_1/rs";
    }
    public static class ActionsCleaner {
        public static String DADATA_POST_RECORD = "clean/";
        public static String DADATA_POST_ADDRESS = "clean/address";
    }
    public static class ActionsSuggestions {
        public static String DADATA_POST_SUGGEST = "/suggest/postal_unit";
        public static String DADATA_POST_FIND_BY_ID = "/findById/postal_unit";
        public static String DADATA_POST_GEOLOCATE = "/geolocate/postal_unit";
    }

}
