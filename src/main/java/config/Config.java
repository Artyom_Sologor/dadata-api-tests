package config;
import constants.AuthConstants;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import static constants.Constants.RunVariable.*;


public class Config {

    // Specifications for DatataCleaner tests
    protected RequestSpecification baseSpecCleaner = new RequestSpecBuilder()
            .setBaseUri(serverCleaner)
            .setBasePath(pathCleaner)
            .setContentType("application/json;charset=utf-8")
            .addHeader("Authorization", AuthConstants.API_KEY)
            .addHeader("X-Secret", AuthConstants.X_SECRET)
            .build();

    protected RequestSpecification caseInsensitivityCleanerSpec = new RequestSpecBuilder()
            .setBaseUri(serverCleaner)
            .setBasePath(pathCleaner)
            .setContentType("Application/Json;Charset=Utf-8")
            .addHeader("authorization", AuthConstants.API_KEY)
            .addHeader("x-Secret", AuthConstants.X_SECRET)
            .build();

    protected RequestSpecification noApiKeyHeaderCleanerSpec = new RequestSpecBuilder()
            .setBaseUri(serverCleaner)
            .setBasePath(pathCleaner)
            .setContentType("application/json;charset=utf-8")
            .addHeader("X-Secret", AuthConstants.X_SECRET)
            .build();

    protected RequestSpecification noSecretKeyHeaderCleanerSpec = new RequestSpecBuilder()
            .setBaseUri(serverCleaner)
            .setBasePath(pathCleaner)
            .setContentType("application/json;charset=utf-8")
            .addHeader("Authorization", AuthConstants.API_KEY)
            .build();

    protected RequestSpecification noContentTypeHeaderCleanerSpec = new RequestSpecBuilder()
            .setBaseUri(serverCleaner)
            .setBasePath(pathCleaner)
            .addHeader("Authorization", AuthConstants.API_KEY)
            .addHeader("X-Secret", AuthConstants.X_SECRET)
            .build();

    // Specifications for DatataSuggestions tests
    protected RequestSpecification baseSpecSuggestions = new RequestSpecBuilder()
            .setBaseUri(serverSuggestions)
            .setBasePath(pathSuggestions)
            .setContentType("application/json")
            .addHeader("Authorization", AuthConstants.API_KEY)
            .addHeader("X-Secret", AuthConstants.X_SECRET)
            .build();

    protected RequestSpecification caseInsensitivitySuggestionsSpec  = new RequestSpecBuilder()
            .setBaseUri(serverSuggestions)
            .setBasePath(pathSuggestions)
            .setContentType("Application/Json;Charset=Utf-8")
            .addHeader("authorization", AuthConstants.API_KEY)
            .addHeader("x-Secret", AuthConstants.X_SECRET)
            .build();

    protected RequestSpecification noApiKeyHeaderSuggestionsSpec = new RequestSpecBuilder()
            .setBaseUri(serverSuggestions)
            .setBasePath(pathSuggestions)
            .setContentType("application/json;charset=utf-8")
            .addHeader("X-Secret", AuthConstants.X_SECRET)
            .build();

    protected RequestSpecification noSecretKeyHeaderSuggestionsSpec = new RequestSpecBuilder()
            .setBaseUri(serverSuggestions)
            .setBasePath(pathSuggestions)
            .setContentType("application/json;charset=utf-8")
            .addHeader("Authorization", AuthConstants.API_KEY)
            .build();

    protected RequestSpecification noContentTypeHeaderSuggestionsSpec = new RequestSpecBuilder()
            .setBaseUri(serverSuggestions)
            .setBasePath(pathSuggestions)
            .addHeader("Authorization", AuthConstants.API_KEY)
            .addHeader("X-Secret", AuthConstants.X_SECRET)
            .build();


}
