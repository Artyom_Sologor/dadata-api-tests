package pojos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressGeoPojo {
    private Double geo_lat;
    private Double geo_lon;

    public Double getGeo_lat() {
        return geo_lat;
    }

    public void setGeo_lat(Double geo_lat) {
        this.geo_lat = geo_lat;
    }

    public Double getGeo_lon() {
        return geo_lon;
    }

    public void setGeo_lon(Double geo_lon) {
        this.geo_lon = geo_lon;
    }
}
