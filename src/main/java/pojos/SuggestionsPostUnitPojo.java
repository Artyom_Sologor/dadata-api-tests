package pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SuggestionsPostUnitPojo {
	@JsonProperty("schedule_wed")
	public String scheduleWed;
	@JsonProperty("address_str")
	public String addressStr;
	@JsonProperty("schedule_mon")
	private String scheduleMon;
	@JsonProperty("schedule_sun")
	private String scheduleSun;
	@JsonProperty("address_kladr_id")
	private String addressKladrId;
	@JsonProperty("address_qc")
	private String addressQc;
	@JsonProperty("schedule_thu")
	private String scheduleThu;
	@JsonProperty("schedule_sat")
	private String scheduleSat;
	@JsonProperty("geo_lat")
	private double geoLat;
	@JsonProperty("schedule_tue")
	private String scheduleTue;
	@JsonProperty("geo_lon")
	private double geoLon;
	@JsonProperty("postal_code")
	private String postalCode;
	@JsonProperty("is_closed")
	private boolean isClosed;
	@JsonProperty("type_code")
	private String typeCode;
	@JsonProperty("schedule_fri")
	private String scheduleFri;


}
