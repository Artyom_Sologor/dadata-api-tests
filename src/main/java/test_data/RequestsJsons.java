package test_data;

public class RequestsJsons {
    // JSONS for TestsCleanerRecord
    public static String recordJson ="{\n" +
            "\"structure\":[\n" +
            "\"NAME\",\n" +
            "\"ADDRESS\",\n" +
            "\"PHONE\"\n" +
            "],\n" +
            "\"data\":[\n" +
            "[\n" +
            "\"Федотов Алексей\",\n" +
            "\"Москва, Сухонская улица, 11 кв 89\",\n" +
            "\"8 916 823 3454\"\n" +
            "]\n" +
            "]\n" +
            "}";
    public static String recordFiveFieldsJson ="{\n" +
            "    \"structure\": [\n" +
            "        \"NAME\",\n" +
            "        \"ADDRESS\",\n" +
            "        \"PHONE\",\n" +
            "        \"PASSPORT\",\n" +
            "\t\t\"EMAIL\"\n" +
            "    ],\n" +
            "    \"data\": [\n" +
            "        [\n" +
            "            \"Федотов Алексей\",\n" +
            "            \"Москва, Сухонская улица, 11 кв 89\",\n" +
            "            \"8 916 823 3454\",\n" +
            "          \t\"KN2411511\",\n" +
            "          \t\"email@email.com\"\n" +
            "        ]\n" +
            "    ]\n" +
            "}";

    public static String recordWithUndocomentFieldJson ="{\n" +
            "    \"structure\": [\n" +
            "        \"NAME\",\n" +
            "        \"ADDRESS\",\n" +
            "        \"PHONE\",\n" +
            "        \"PASSPORT\",\n" +
            "        \"EMAIL\",\n" +
            "        \"HOBBY\"\n" +
            "    ],\n" +
            "    \"data\": [\n" +
            "        [\n" +
            "            \"Федотов Алексей\",\n" +
            "            \"Москва, Сухонская улица, 11 кв 89\",\n" +
            "            \"8 916 823 3454\",\n" +
            "            \"KN2411511\",\n" +
            "            \"email@email.com\",\n" +
            "            \"fishing\"\n" +
            "        ]\n" +
            "    ]\n" +
            "}";
    public static String recordWithAsIsFieldJson ="{\n" +
            "    \"structure\": [\n" +
            "        \"NAME\",\n" +
            "        \"AS_IS\",\n" +
            "        \"PHONE\",\n" +
            "        \"PASSPORT\",\n" +
            "        \"EMAIL\"\n" +
            "    ],\n" +
            "    \"data\": [\n" +
            "        [\n" +
            "            \"Федотов Алексей\",\n" +
            "            \"Москва, Сухонская улица, 11 кв 89\",\n" +
            "            \"8 916 823 3454\",\n" +
            "            \"KN2411511\",\n" +
            "            \"email@email.com\"\n" +
            "        ]\n" +
            "    ]\n" +
            "}";

    public static String recordInvalidJson ="{\n" +
            "structure:[\n" +
            "\"NAME\",\n" +
            "\"ADDRESS\",\n" +
            "\"PHONE\"\n" +
            "],\n" +
            "\"data\":[\n" +
            "[\n" +
            "\"Федотов Алексей\",\n" +
            "\"Москва, Сухонская улица, 11 кв 89\",\n" +
            "\"8 916 823 3454\"\n" +
            "]\n" +
            "]\n" +
            "}";

    public static String recordJsonWithError ="{\n" +
            "\"\":[\n" +
            "\"NAME\",\n" +
            "\"ADDRESS\",\n" +
            "\"PHONE\"\n" +
            "],\n" +
            "\"data\":[\n" +
            "[\n" +
            "\"Федотов Алексей\",\n" +
            "\"Москва, Сухонская улица, 11 кв 89\",\n" +
            "\"8 916 823 3454\"\n" +
            "]\n" +
            "]\n" +
            "}";

    public static String _300CharsRecordJson ="{\n" +
            "\"\":[\n" +
            "\"NAME\",\n" +
            "\"ADDRESS\",\n" +
            "\"PHONE\"\n" +
            "],\n" +
            "\"data\":[\n" +
            "[\n" +
            "\"Федотов Алексей\",\n" +
            "\"siTmgb7tr8K1vL7CfSLOKK3w4ypIwcsDMrbANNAQZFOo176DeAkDChIvnNSlWOjFpw4iMqmBt27CJhbkFQ1NpSmsyqJ1jwEppFQu4VpYEmcZ3Wu32Ey6OIeZSuVIWFEvoES0UrMp0ZUOmWydeuRal9gEOjQOZtmRYasRRrRo54hbZ3L1te3cBgmxkfUpmWRrjKxn0OZJw1MsgyVhXZqkjYdmVDEUU7VJXSGt2FUMHH4LylcUkYX0273jVNBa9IpxMCKspxqWwHvFXURzcbsrHeTkxsbEAQ3WnGsK3h1Gq75F\",\n" +
            "\"8 916 823 3454\"\n" +
            "]\n" +
            "]\n" +
            "}";


    // JSONS for TestsCleanerAddress
    public static String addressJson ="[ \"Москва Сухонская 11\"]";
    public static String addressInvalidJson ="[Москва Сухонская 11]";
    public static String _300CharsAddressJson ="[siTmgb7tr8KvL7CfSLOKK3w4ypIwcsDMrbANNAQZFOo176DeAkDChIvnNSlWOjFpw4iMqmBt27CJhbkFQ1NpSmsyqJ1jwEppFQu4VpYEmcZ3Wu32Ey6OIeZSuVIWFEvoES0UrMp0ZUOmWydeuRal9gEOjQOZtmRYasRRrRo54hbZ3L1te3cBgmxkfUpmWRrjKxn0OZJw1MsgyVhXZqkjYdmVDEUU7VJXSGt2FUMHH4LylcUkYX0273jVNBa9IpxMCKspxqWwHvFXURzcbsrHeTkxsbEAQ3WnGsK3h1Gq75F8]";

    // JSONS for TestsSuggestionsPostalUnit
    public static String suggestJson = "{ \"query\": \"дежнева 2а\" }";

    public static String _299CharsPostalUnitJson = "{ \"query\": \"siTmgb7tr8KvL7CfSLOKK3w4ypIwcsDMrbANNAQZFOo176DeAkDChIvnNSlWOjFpw4iMqmBt27CJhbkFQ1NpSmsyqJ1jwEppFQu4VpYEmcZ3Wu32Ey6OIeZSuVIWFEvoES0UrMp0ZUOmWydeuRal9gEOjQOZtmRYasRRrRo54hbZ3L1te3cBgmxkfUpmWRrjKxn0OZJw1MsgyVhXZqkjYdmVDEUU7VJXSGt2FUMHH4LylcUkYX0273jVNBa9IpxMCKspxqWwHvFXURzcbsrHeTkxsbEAQ3WnGsK3h1Gq75F\" }";
    public static String _300CharsPostalUnitJson = "{ \"query\": \"siTmgb7tr8KvL7CfSLOKK3w4ypIwcsDMrbANNAQZFOo176DeAkDChIvnNSlWOjFpw4iMqmBt27CJhbkFQ1NpSmsyqJ1jwEppFQu4VpYEmcZ3Wu32Ey6OIeZSuVIWFEvoES0UrMp0ZUOmWydeuRal9gEOjQOZtmRYasRRrRo54hbZ3L1te3cBgmxkfUpmWRrjKxn0OZJw1MsgyVhXZqkjYdmVDEUU7VJXSGt2FUMHH4LylcUkYX0273jVNBa9IpxMCKspxqWwHvFXURzcbsrHeTkxsbEAQ3WnGsK3h1Gq75F8\" }";
    public static String _301CharsPostalUnitJson = "{ \"query\": \"siTmgb7tr8KvL7CfSLOKK3w4ypIwcsDMrbANNAQZFOo176DeAkDChIvnNSlWOjFpw4iMqmBt27CJhbkFQ1NpSmsyqJ1jwEppFQu4VpYEmcZ3Wu32Ey6OIeZSuVIWFEvoES0UrMp0ZUOmWydeuRal9gEOjQOZtmRYasRRrRo54hbZ3L1te3cBgmxkfUpmWRrjKxn0OZJw1MsgyVhXZqkjYdmVDEUU7VJXSGt2FUMHH4LylcUkYX0273jVNBa9IpxMCKspxqWwHvFXURzcbsrHeTkxsbEAQ3WnGsK3h1Gq75F81\" }";

    public static String findByIdJson = "{ \"query\": \"127642\" }";

    public static String geolocateJson = "{ \"lat\": 55.878, \"lon\": 37.653, \"radius_meters\": 1000 }";


    public static String geolocateWithAddressAndFilterJson = "{\n" +
            "  \"query\": \"105\",\n" +
            "  \"filters\": [\n" +
            "    {\n" +
            "      \"is_closed\": false\n" +
            "    }\n" +
            "  ]\n" +
            "}";

}
